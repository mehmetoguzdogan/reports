﻿using System;
using Report.Queuing.Model.Attributes;

namespace Report.Queuing.Model.QueueModel
{
    [ExchangeName("ReportQueueExchange")]
    [QueueName("ReportQueue")]
    [Serializable]
    public class ReportQueueModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Firma.
        /// </summary>
        public Guid ReportId { get; set; }

        #endregion
    }
}

