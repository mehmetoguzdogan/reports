﻿using System;
namespace Report.Queuing.Model.Const
{
    public static class QueueSettingsConst
    {
        #region Constants

        /// <summary>
        /// Defines the DEVCONNECTIONS.
        /// </summary>
        public const string DEVCONNECTIONS = "localhost";

        /// <summary>
        /// Defines the DEVPASSWORD.
        /// </summary>
        public const string DEVPASSWORD = "guest";

        /// <summary>
        /// Defines the DEVUSERNAME.
        /// </summary>
        public const string DEVUSERNAME = "guest";  //!oguz?dogan!.

        #endregion
    }
}

