﻿using System;
using System.Linq;

namespace Report.Queuing.Extension
{
    public static class AttributeExtension
    {
        #region Methods

        /// <summary>
        /// The GetAttribute
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="type">The type<see cref="Type"/></param>
        /// <param name="valueSelector">The valueSelector<see cref="Func{TAttribute, TValue}"/></param>
        /// <param name="inherit">The inherit<see cref="bool"/></param>
        /// <returns>The <see cref="TValue"/></returns>
        public static TValue GetAttribute<TAttribute, TValue>(this Type type, Func<TAttribute, TValue> valueSelector, bool inherit = false) where TAttribute : Attribute
        {
            TAttribute att = type.GetCustomAttributes(typeof(TAttribute), inherit).FirstOrDefault() as TAttribute;

            if (att != null)
            {
                return valueSelector(att);
            }

            return default(TValue);
        }

        /// <summary>
        /// The GetAttributeFrom
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type">The type<see cref="Type"/></param>
        /// <param name="propertyName">The propertyName<see cref="string"/></param>
        /// <returns>The <see cref="T"/></returns>
        public static T GetAttributeFrom<T>(this Type type, string propertyName) where T : Attribute
        {
            var attrType = typeof(T);
            var property = type.GetProperty(propertyName);
            return (T)property.GetCustomAttributes(attrType, false).First();
        }

        /// <summary>
        /// The GetAttributeValue
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="type">The type<see cref="Type"/></param>
        /// <param name="valueSelector">The valueSelector<see cref="Func{TAttribute, TValue}"/></param>
        /// <returns>The <see cref="TValue"/></returns>
        public static TValue GetAttributeValue<TAttribute, TValue>(this Type type, Func<TAttribute, TValue> valueSelector) where TAttribute : Attribute
        {
            var att = type.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault() as TAttribute;

            if (att != null)
            {
                return valueSelector(att);
            }

            return default(TValue);
        }

        #endregion
    }
}

