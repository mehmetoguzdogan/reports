﻿using System;
using EasyNetQ.SystemMessages;
using Newtonsoft.Json;
using Report.Queuing.Extension;
using Report.Queuing.Model.Attributes;
using System.Text;
using EasyNetQ.Topology;
using EasyNetQ;


namespace Report.Queuing
{
    public static class Queue
    {
        #region Methods

        /// <summary>
        /// The FuturePublish.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="message">The message<see cref="T"/>.</param>
        /// <param name="date">The date<see cref="DateTime"/>.</param>
        /// <param name="exchangeType">The exchangeType<see cref="string"/>.</param>
        /// <param name="routingKey">The routingKey<see cref="string"/>.</param>
        public static void FuturePublish<T>(T message, string exchangeType = ExchangeType.Topic, string routingKey = "") where T : class
            => BusFactory.GetBus<T>(false, 0, exchangeType, routingKey).Publish(message);

        /// <summary>
        /// The Publish.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="message">The message<see cref="T"/>.</param>
        /// <param name="exchangeType">The exchangeType<see cref="string"/>.</param>
        /// <param name="routingKey">The routingKey<see cref="string"/>.</param>
        public static void Publish<T>(T message, string exchangeType = ExchangeType.Topic, string routingKey = "") where T : class
            => BusFactory.GetBus<T>(false, 0, exchangeType, routingKey).Publish(message);

        /// <summary>
        /// The Subscribe.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="onMessage">The onMessage<see cref="Action{T}"/>.</param>
        /// <param name="prefetchCount">The prefetchCount<see cref="int"/>.</param>
        /// <param name="subscriptionId">The subscriptionId<see cref="string"/>.</param>
        /// <param name="exchangeType">The exchangeType<see cref="string"/>.</param>
        /// <param name="routingKey">The routingKey<see cref="string"/>.</param>
        public static void Subscribe<T>(Action<T> onMessage, int prefetchCount = 0, string subscriptionId = "", string exchangeType = ExchangeType.Topic, string routingKey = "") where T : class
            => BusFactory.GetBus<T>(true, prefetchCount, exchangeType, routingKey)?.Subscribe<T>(subscriptionId, x => onMessage(x),
               config => config?.WithTopic(typeof(T).GetAttributeValue((ExchangeNameAttribute exName) => exName.ExchangeName)));

        /// <summary>
        /// The SubscribeError.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="onMessage">The onMessage<see cref="Action{T}"/>.</param>
        /// <param name="prefetchCount">The prefetchCount<see cref="int"/>.</param>
        /// <param name="subscriptionId">The subscriptionId<see cref="string"/>.</param>
        /// <param name="exchangeType">The exchangeType<see cref="string"/>.</param>
        /// <param name="routingKey">The routingKey<see cref="string"/>.</param>
        public static void SubscribeError<T>(Action<Error> onMessage, int prefetchCount = 0) where T : class
        {
            var bus = BusFactory.GetErrorBus<T>(prefetchCount);
            string queueName = $"{typeof(T).GetAttributeValue((QueueNameAttribute queueAttribute) => queueAttribute.QueueName)}.Error";
            var queue = bus?.Advanced?.QueueDeclare(queueName);
            bus.Advanced.Consume(queue, (x, messageProperties, messageReceivedInfo) =>
            {
                onMessage(JsonConvert.DeserializeObject<Error>(Encoding.UTF8.GetString(x)));
            });
        }

        #endregion
    }
}

