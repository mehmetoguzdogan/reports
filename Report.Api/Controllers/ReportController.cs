﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Report.Repository.Enum;
using Report.Repository.Model;
using Report.Service.Report;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Report.Api.Controllers
{
    [Route("api/[controller]")]
    public class ReportController : Controller
    {

        private readonly IReportRequestService _requestService;
        private readonly IReportsService _reportService;

        public ReportController(IReportRequestService requestService, IReportsService reportService)
        {
            _requestService = requestService;
            _reportService = reportService;
        }

        // GET: api/values
        [HttpGet]
        public List<ReportRequest> Get()
        {
            return _requestService.GetList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ReportRequest Get(Guid id)
        {
            return _requestService.Get(id);
        }

        // POST api/values
        [HttpPost]
        public Guid Post([FromBody] ReportRequest report)
        {
            return _requestService.Add(report);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ReportRequest Put(Guid id, [FromBody] ReportRequest report)
        {
            return _requestService.Update(id,report);
        }

        [HttpPut]
        public Guid Put([FromQuery]Guid id, [FromQuery] ReportStatu statu)
        {
            return _requestService.UpdateStatu(id, statu).Id;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public bool Delete(Guid id)
        {
            return _requestService.Delete(id);
        }

        [HttpPost]
        [Route("ReportsAdd")]
        public void ReportsAdd([FromBody]List<Reports> reports)
        {
            _reportService.AddList(reports);
        }

        [HttpGet]
        [Route("GetReport")]
        public List<Reports> GetReport([FromQuery]Guid requestId)
        {
            return _reportService.GetReport(requestId);
        }
    }
}

