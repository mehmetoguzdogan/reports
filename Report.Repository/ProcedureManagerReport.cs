﻿using System;
using Report.Infrastructure.Repository;

namespace Report.Repository
{
    public class ProcedureManagerReport : ProcedureManager, IProcedureManagerReport
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcedureManagerReport"/> class.
        /// </summary>
        /// <param name="context">The context<see cref="IReportContext"/>.</param>
        public ProcedureManagerReport(IReportContext context) : base(context)
        {
        }

        #endregion
    }
}

