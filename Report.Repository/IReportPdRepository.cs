﻿using System;
using Report.Infrastructure.Entity;
using Report.Infrastructure.Repository;

namespace Report.Repository
{
    public interface IReportPdRepository<T, TId> : IRepository<T, TId> where T : IBaseEntity<TId>
    {
    }
}

