﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Report.Repository.Model;

namespace Report.Repository.Map
{
    public class ReportsMap : IEntityTypeConfiguration<Reports>
    {
        #region Methods

        /// <summary>
        /// The Configure.
        /// </summary>
        /// <param name="builder">The builder<see cref="EntityTypeBuilder{FreezeModel}"/>.</param>
        public void Configure(EntityTypeBuilder<Reports> builder)
        {
            //Keys
            builder.HasKey(x => x.Id);

            //Table
            builder.ToTable("reports");

            //Properties
            builder.Property(x => x.Id).HasColumnName("id").IsRequired();
            builder.Property(x => x.RequestId).HasColumnName("reportrequestid").IsRequired();
            builder.Property(x => x.Location).HasColumnName("location").IsRequired();
            builder.Property(x => x.PersonCount).HasColumnName("personcount").IsRequired();
            builder.Property(x => x.PhoneNumberCount).HasColumnName("phonenumbercount").IsRequired();
            builder.Property(x => x.IsActive).HasColumnName("isactive").IsRequired();
        }

        #endregion
    }
}

