﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Report.Repository.Model;

namespace Report.Repository.Map
{
    public class ReportRequestMap : IEntityTypeConfiguration<ReportRequest>
    {
        #region Methods

        /// <summary>
        /// The Configure.
        /// </summary>
        /// <param name="builder">The builder<see cref="EntityTypeBuilder{ReportRequest}"/>.</param>
        public void Configure(EntityTypeBuilder<ReportRequest> builder)
        {
            //Keys
            builder.HasKey(x => x.Id);

            //Table
            builder.ToTable("reportrequests");

            //Properties
            builder.Property(x => x.Id).HasColumnName("id").IsRequired();
            builder.Property(x => x.RequestDate).HasColumnName("requestdate").IsRequired();
            builder.Property(x => x.Statu).HasColumnName("statu").IsRequired();
            builder.Property(x => x.UpdateDate).HasColumnName("updatedate");
            builder.Property(x => x.IsActive).HasColumnName("isactive").IsRequired();
        }

        #endregion
    }
}

