﻿using System;
using Report.Infrastructure.Entity;
using Report.Infrastructure.Repository;

namespace Report.Repository
{
    public class ReportPdRepository<T, TId> : ReportRepository<T, TId>, IReportPdRepository<T, TId> where T : class, IBaseEntity<TId>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportPdRepository{T, TId}"/> class.
        /// </summary>
        /// <param name="ReportContext">The TnProductFreezeContext<see cref="IReportContext"/>.</param>
        public ReportPdRepository(IReportContext ReportContext) : base((IContext)ReportContext)
        {
        }

        #endregion
    }
}

