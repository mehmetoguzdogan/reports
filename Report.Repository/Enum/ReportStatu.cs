﻿using System;
using System.ComponentModel;

namespace Report.Repository.Enum
{
    public enum ReportStatu
    {
        [Description("Talep Alındı")]
        requestreceived,
        [Description("Hazırlanıyor")]
        preparing,
        [Description("Hazırlandı")]
        prepared
    }
}

