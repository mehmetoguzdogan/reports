﻿using System;
using System.ComponentModel;

namespace Report.Repository.Enum
{
    public enum InformationType
    {
        [Description("TelefonNumarası")]
        PhoneNumber,
        [Description("Eposta")]
        Email,
        [Description("Konum")]
        Location
    }
}

