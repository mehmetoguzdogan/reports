﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Report.Infrastructure.Engine;
using Report.Repository;
using Report.Infrastructure.Logs;

namespace Report.Repository.Engine
{
    public class PosgreRegister : IDynamicRegister
    {
        #region Methods

        /// <summary>
        /// The Configure.
        /// </summary>
        /// <param name="service">The service<see cref="IServiceCollection"/>.</param>
        public void Configure(IServiceCollection service)
        {
            service.AddTransient<IReportContext>(f => { return new ReportContext(f.GetService<ConsoleLoggerFactory>().LoggerFactory); });
            service.AddScoped(typeof(IReportRepository<,,>), typeof(ReportRepository<,,>));
            service.AddScoped(typeof(IReportPdRepository<,>), typeof(ReportPdRepository<,>));
            service.AddScoped(typeof(IProcedureManagerReport), typeof(ProcedureManagerReport));
            service.AddEntityFrameworkNpgsql();
        }

        #endregion
    }
}

