﻿using System;
using Report.Infrastructure.Entity;

namespace Report.Repository.Model
{
    public class ReportRequest : IBaseEntity<Guid>
    {
        public Guid Id { get; set; }
        public DateTime RequestDate { get; set; }
        public int Statu { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int IsActive { get; set; }
    }
}

