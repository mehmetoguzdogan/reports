﻿using System;
using Report.Infrastructure.Entity;

namespace Report.Repository.Model
{
    public class Reports : IBaseEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid RequestId { get; set; }
        public string Location { get; set; }
        public int PersonCount { get; set; }
        public int PhoneNumberCount { get; set; }
        public int IsActive { get; set; }
    }
}

