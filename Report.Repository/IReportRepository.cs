﻿using System;
using Report.Infrastructure.Entity;
using Report.Infrastructure.Repository;

namespace Report.Repository
{
    public interface IReportRepository<T, TId, TUserId> : IRepository<T, TId> where T : IEntity<TId, TUserId>
    {
    }
}

