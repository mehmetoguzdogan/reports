﻿using System;
using Report.Infrastructure.Entity;
using Report.Infrastructure.Repository;

namespace Report.Repository
{
    public class ReportRepository<T, TId, TUserId> : Repository<T, TId, TUserId>, IReportRepository<T, TId, TUserId> where T : class, IEntity<TId, TUserId>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportRepository{T, TId, TUserId}"/> class.
        /// </summary>
        /// <param name="context">The context<see cref="IReportContext"/>.</param>
        /// <param name="user">The user<see cref="IUser{TUserId}"/>.</param>
        public ReportRepository(IReportContext context, IUser<TUserId> user) : base(context, user)
        {
        }

        #endregion
    }
}

