﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Report.Infrastructure.DbContext;
using Report.Repository.Const;
using Report.Repository.Map;
using Report.Repository.Model;

namespace Report.Repository
{
    public class ReportContext : PostgreDbContext, IReportContext
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportContext"/> class.
        /// </summary>
        /// <param name="loggerFactory">The loggerFactory<see cref="ILoggerFactory"/>.</param>
        public ReportContext(ILoggerFactory loggerFactory) : base(ConnectionConst.PosgreConnection, loggerFactory, "public")
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ReportRequest.
        /// </summary>
        public DbSet<ReportRequest> ReportRequests { get; set; }
        public DbSet<Reports> Reports { get; set; }


        #endregion

        #region Methods

        /// <summary>
        /// The OnModelCreating.
        /// </summary>
        /// <param name="modelBuilder">The modelBuilder<see cref="ModelBuilder"/>.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ReportRequestMap());
            modelBuilder.ApplyConfiguration(new ReportsMap());
        }

        #endregion
    }
}

