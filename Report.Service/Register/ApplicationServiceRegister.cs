﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Report.Infrastructure.Engine;
using Report.Service.Report;

namespace Report.Service.Register
{
    public class ApplicationServiceRegister : IDynamicRegister
    {
        #region Methods

        /// <summary>
        /// The Configure.
        /// </summary>
        /// <param name="service">The service<see cref="IServiceCollection"/>.</param>
        public void Configure(IServiceCollection service)
        {
            service.AddTransient<IReportRequestService, ReportRequestService>();
            service.AddTransient<IReportsService, ReportsService>();
        }

        #endregion
    }
}

