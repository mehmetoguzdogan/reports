﻿using System;
using Report.Repository;
using Report.Repository.Model;

namespace Report.Service.Report
{
    public class ReportsService : IReportsService
    {
        private readonly IReportPdRepository<Reports, Guid> _reports;

        public ReportsService(IReportPdRepository<Reports, Guid> reports)
        {
            _reports = reports;
        }

        public void AddList(List<Reports> reports)
        {
            _reports.Add(reports);
        }

        public List<Reports> GetReport(Guid requestId)
        {
            return _reports.FindAll(x => x.IsActive == default(int) && x.RequestId == requestId).ToList();
        }
    }
}

