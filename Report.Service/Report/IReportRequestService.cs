﻿using System;
using Report.Repository.Enum;
using Report.Repository.Model;

namespace Report.Service.Report
{
    public interface IReportRequestService
    {
        Guid Add(ReportRequest report);
        ReportRequest Update(Guid id, ReportRequest report);
        bool Delete(Guid id);
        ReportRequest Get(Guid id);
        List<ReportRequest> GetList();
        ReportRequest UpdateStatu(Guid id, ReportStatu reportStatu);
    }
}

