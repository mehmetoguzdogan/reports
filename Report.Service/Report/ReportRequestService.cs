﻿using System;
using Report.Repository;
using Report.Repository.Enum;
using Report.Repository.Model;
using Report.Queuing;
using Report.Queuing.Model.QueueModel;

namespace Report.Service.Report
{
    public class ReportRequestService : IReportRequestService
    {
        private readonly IReportPdRepository<ReportRequest, Guid> _reportRequest;

        public ReportRequestService(IReportPdRepository<ReportRequest, Guid> reportRequest)
        {
            _reportRequest = reportRequest;
        }

        public Guid Add(ReportRequest report)
        {
            report.RequestDate = DateTime.SpecifyKind(DateTime.Now,DateTimeKind.Utc);
            Guid reportId = _reportRequest.Add(report);
            Queuing.Queue.Publish(new ReportQueueModel { ReportId = reportId });
            return reportId;
        }

        public bool Delete(Guid id)
        {
            return _reportRequest.Delete(id);
        }

        public ReportRequest Get(Guid id)
        {
            return _reportRequest.Get(id);
        }

        public List<ReportRequest> GetList()
        {
            return _reportRequest.FindAll(x=>x.IsActive == default(int)).ToList();
        }

        public ReportRequest Update(Guid id, ReportRequest report)
        {
            report.RequestDate = DateTime.SpecifyKind(report.RequestDate, DateTimeKind.Utc);
            report.UpdateDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);
            return _reportRequest.Update(id,report);
        }

        public ReportRequest UpdateStatu(Guid id, ReportStatu reportStatu)
        {
            ReportRequest report = _reportRequest.Get(id);
            report.Statu = (int)reportStatu;
            report.RequestDate = DateTime.SpecifyKind(report.RequestDate, DateTimeKind.Utc);
            report.UpdateDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc);

            return _reportRequest.Update(id,report);
        }

    }
}

