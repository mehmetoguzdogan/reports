﻿using System;
using Report.Repository.Model;

namespace Report.Service.Report
{
    public interface IReportsService
    {
        void AddList(List<Reports> reports);
        List<Reports> GetReport(Guid requestId);
    }
}

