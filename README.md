

Net 6 da gelistirilmis net 6 sdk yüklü olması gerekmekte

PostgreSqlde projeyi actıgınızda 
Report.Repository.Const altında bulunan ConnectionConst'taki 
connection string bilgisi degıstırılecek ve daha sonrasında asagıda bulunan ıkı tabloyu postgre'de olusturulması gerekıyor. 


ek olarak rabbitmq ayarları ıcın 
Report.Queuing.Model.Const altında bulunan QueueSettingsConst bilgilerinin guncellenmesi gerekıyor. 

---------------------------
-- Table: public.reportrequests

-- DROP TABLE IF EXISTS public.reportrequests;

CREATE TABLE IF NOT EXISTS public.reportrequests
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    requestdate timestamp without time zone NOT NULL,
    statu integer NOT NULL,
    updatedate timestamp without time zone,
    isactive integer NOT NULL DEFAULT 1
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.reportrequests
    OWNER to postgres;
-----------------------------

-- Table: public.reports

-- DROP TABLE IF EXISTS public.reports;

CREATE TABLE IF NOT EXISTS public.reports
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    reportrequestid uuid NOT NULL,
    location text COLLATE pg_catalog."default" NOT NULL,
    personcount integer NOT NULL,
    phonenumbercount integer NOT NULL,
    isactive integer NOT NULL DEFAULT 1
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.reports
    OWNER to postgres;


-----------------------------





ReportController'e post metodu ile istekte bulundugunuzda yeni bir rapor talebı olusturuyor. 
Talep ıcın verıtabanına kayıt attıktan sonra. 
Rabbitmq'a eklenen kaydın Id'si reportqueue'ye eklenıyor. 
Buradan sonra Phone.Directory Projesinde consumer tarafında taleple ılgılı guncellenmeler gerceklesıyor.

Raporun verısı hazırlandıgına ReportControllerda bulunan ReportsAdd metoduna ıstek atarak rapordakı verıler mevcut tabloya eklenıyor.
Daha sonrasında Reportcontroller/GetReport'a talep id'si ile istek atıldıgında rapor verılerı liste halınde ıletılıyor. 
