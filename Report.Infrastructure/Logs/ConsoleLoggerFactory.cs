﻿using System;
using Microsoft.Extensions.Logging;

namespace Report.Infrastructure.Logs
{
    public class ConsoleLoggerFactory
    {
        #region Fields

        /// <summary>
        /// Defines the loggerFactory
        /// </summary>
        private readonly ILoggerFactory loggerFactory;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsoleLoggerFactory"/> class.
        /// </summary>
        /// <param name="loggerFactory">The loggerFactory<see cref="ILoggerFactory"/></param>
        public ConsoleLoggerFactory(ILoggerFactory loggerFactory) => this.loggerFactory = loggerFactory;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the LoggerFactory
        /// </summary>
        public ILoggerFactory LoggerFactory => loggerFactory;

        #endregion
    }
}

