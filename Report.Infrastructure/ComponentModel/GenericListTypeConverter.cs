﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Text;

namespace Report.Infrastructure.ComponentModel
{
    public class GenericListTypeConverter<T> : TypeConverter
    {
        #region Fields

        /// <summary>
        /// The type converter.
        /// </summary>
        protected readonly TypeConverter typeConverter;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericListTypeConverter{T}"/> class.
        /// </summary>
        public GenericListTypeConverter()
        {
            this.typeConverter = TypeDescriptor.GetConverter(typeof(T));

            if (this.typeConverter == null)
            {
                throw new InvalidOperationException("No type converter exists for type " + typeof(T).FullName);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The can convert from.
        /// </summary>
        /// <param name="context"> The context. </param>
        /// <param name="sourceType"> The source type. </param>
        /// <returns> The <see cref="bool"/>. </returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType != typeof(string))
            {
                return base.CanConvertFrom(context, sourceType);
            }

            var items = this.GetStringArray(sourceType.ToString());

            return items.Any();
        }

        /// <summary>
        /// The convert from.
        /// </summary>
        /// <param name="context"> The context. </param>
        /// <param name="culture"> The culture. </param>
        /// <param name="value"> The value. </param>
        /// <returns> The <see cref="object"/>. </returns>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (!(value is string))
            {
                return base.ConvertFrom(context, culture, value);
            }

            var items = this.GetStringArray((string)value);
            var result = new List<T>();
            Array.ForEach(
                items,
                s =>
                {
                    var item = this.typeConverter.ConvertFromInvariantString(s);

                    if (item != null)
                    {
                        result.Add((T)item);
                    }
                });

            return result;
        }

        /// <summary>
        /// The convert to.
        /// </summary>
        /// <param name="context"> The context. </param>
        /// <param name="culture"> The culture. </param>
        /// <param name="value"> The value. </param>
        /// <param name="destinationType"> The destination type. </param>
        /// <returns> The <see cref="object"/>. </returns>
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType != typeof(string))
            {
                return base.ConvertTo(context, culture, value, destinationType);
            }

            StringBuilder result = new StringBuilder();

            if (value == null)
            {
                return result.ToString();
            }

            for (int i = 0; i < ((IList<T>)value).Count; i++)
            {
                var str1 = Convert.ToString(((IList<T>)value)[i], CultureInfo.InvariantCulture);
                result.Append(str1);

                if (i != ((IList<T>)value).Count - 1)
                {
                    result.Append(",");
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// The get string array.
        /// </summary>
        /// <param name="input"> The input. </param>
        /// <returns> The <see cref="string"/>. </returns>
        protected virtual string[] GetStringArray(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return new string[0];
            }

            var result = input.Split(',');
            // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
            Array.ForEach(result, s => s.Trim());

            return result;
        }

        #endregion
    }
}

