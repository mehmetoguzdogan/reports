﻿using System;
namespace Report.Infrastructure.Entity
{
    public interface IBaseEntity<TId>
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        TId Id { get; set; }

        #endregion
    }
}

