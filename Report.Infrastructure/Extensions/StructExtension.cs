﻿using System;
namespace Report.Infrastructure.Extensions
{
    public static class StructExtension
    {
        #region Methods

        /// <summary>
        /// The GetMonthSequence.
        /// </summary>
        /// <param name="tarih">The tarih<see cref="DateTime"/>.</param>
        /// <returns>The <see cref="int"/>.</returns>
        public static int GetMonthSequence(this DateTime tarih)
        {
            DateTime from = new DateTime(2005, 1, 1);

            return ((tarih.Year - from.Year) * 12) + tarih.Month - from.Month + 38;
        }

        /// <summary>
        /// The RoundToMoney.
        /// </summary>
        /// <param name="value">The value<see cref="double"/>.</param>
        /// <returns>The <see cref="Double"/>.</returns>
        public static decimal RoundToMoney(this decimal value)
            => (value * 1000000 % 10000) == 5000 ? Math.Round(value, 2) - (decimal)0.01 : Math.Round(value, 2);

        #endregion
    }
}

