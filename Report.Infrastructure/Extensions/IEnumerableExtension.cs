﻿using System;
namespace Report.Infrastructure.Extensions
{
    public static class IEnumerableExtension
    {
        #region Methods

        /// <summary>
        /// The ForEach
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source<see cref="IEnumerable{T}"/></param>
        /// <param name="action">The action<see cref="Action{T}"/></param>
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source != null && source.Any())
            {
                foreach (T item in source)
                {
                    action(item);
                }
            }
        }

        #endregion
    }
}

