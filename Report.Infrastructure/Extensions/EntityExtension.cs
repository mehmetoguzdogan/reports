﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Report.Infrastructure.Helpers;

namespace Report.Infrastructure.Extensions
{
    public static class EntityExtension
    {
        #region Methods

        /// <summary>
        /// toList nolock.
        /// </summary>
        /// <typeparam name="T"> Generic type. .</typeparam>
        /// <param name="query"> The query. .</param>
        /// <returns> Nolock list. .</returns>
        public static bool AnyNoLock<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return query.Any();
            }
        }

        /// <summary>
        /// The any no lock.
        /// </summary>
        /// <typeparam name="T"> Generic Type .</typeparam>
        /// <param name="query"> The query. .</param>
        /// <param name="filter"> The filter. .</param>
        /// <returns> The <see cref="bool"/>. .</returns>
        public static bool AnyNoLock<T>(this IQueryable<T> query, Expression<Func<T, bool>> filter)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return query.Any(filter);
            }
        }

        /// <summary>
        /// The AnyNoLockAsync.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="query">The query<see cref="IQueryable{T}"/>.</param>
        /// <param name="filter">The filter<see cref="Expression{Func{T, bool}}"/>.</param>
        /// <returns>The <see cref="Task{bool}"/>.</returns>
        public static async Task<bool> AnyNoLockAsync<T>(this IQueryable<T> query, Expression<Func<T, bool>> filter)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return await query.AnyAsync(filter);
            }
        }

        /// <summary>
        /// The CountNoLock.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="query">The query<see cref="IQueryable{T}"/>.</param>
        /// <returns>The <see cref="int"/>.</returns>
        public static int CountNoLock<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return query.Count();
            }
        }

        /// <summary>
        /// The CountNoLockAsync.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="query">The query<see cref="IQueryable{T}"/>.</param>
        /// <returns>The <see cref="Task{int}"/>.</returns>
        public static async Task<int> CountNoLockAsync<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return await query.CountAsync();
            }
        }

        /// <summary>
        /// toList nolock.
        /// </summary>
        /// <typeparam name="T"> Generic type. .</typeparam>
        /// <param name="query"> The query. .</param>
        /// <returns> Nolock list. .</returns>
        public static T FirstOrDefaultNoLock<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return query.FirstOrDefault();
            }
        }

        /// <summary>
        /// The FirstOrDefaultNoLockAsync.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="query">The query<see cref="IQueryable{T}"/>.</param>
        /// <returns>The <see cref="Task{T}"/>.</returns>
        public static async Task<T> FirstOrDefaultNoLockAsync<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return await query.FirstOrDefaultAsync();
            }
        }

        /// <summary>
        /// toList nolock.
        /// </summary>
        /// <typeparam name="T"> Generic type. .</typeparam>
        /// <param name="query"> The query. .</param>
        /// <returns> Nolock list. .</returns>
        public static T LastOrDefaultNoLock<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return query.LastOrDefault();
            }
        }

        /// <summary>
        /// The LastOrDefaultNoLockAsync.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="query">The query<see cref="IQueryable{T}"/>.</param>
        /// <returns>The <see cref="Task{T}"/>.</returns>
        public static async Task<T> LastOrDefaultNoLockAsync<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return await query.LastOrDefaultAsync();
            }
        }

        /// <summary>
        /// toList nolock.
        /// </summary>
        /// <typeparam name="T"> Generic type. .</typeparam>
        /// <param name="query"> The query. .</param>
        /// <returns> Nolock list. .</returns>
        public static T SingleOrDefaultNoLock<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return query.SingleOrDefault();
            }
        }

        /// <summary>
        /// toList nolock.
        /// </summary>
        /// <typeparam name="T"> Generic type. .</typeparam>
        /// <param name="query"> The query. .</param>
        /// <returns> Nolock list. .</returns>
        public static async Task<T> SingleOrDefaultNoLockAsync<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return await query.SingleOrDefaultAsync();
            }
        }

        /// <summary>
        /// ToList nolock.
        /// </summary>
        /// <typeparam name="T"> Generic type. .</typeparam>
        /// <param name="query"> The query. .</param>
        /// <returns> Nolock list. .</returns>
        public static List<T> ToListNoLock<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return query.ToList();
            }
        }

        /// <summary>
        /// ToList nolock.
        /// </summary>
        /// <typeparam name="T"> Generic type. .</typeparam>
        /// <param name="query"> The query. .</param>
        /// <returns> Nolock list. .</returns>
        public static async Task<List<T>> ToListNoLockAsync<T>(this IQueryable<T> query)
        {
            using (TransactionHelper.CreateNoLockTransaction())
            {
                return await query.ToListAsync();
            }
        }

        #endregion
    }
}

