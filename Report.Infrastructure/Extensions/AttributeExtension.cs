﻿using System;
using Report.Infrastructure.Attributes;
using System.ComponentModel;
using System.Reflection;

namespace Report.Infrastructure.Extensions
{
    public static class AttributeExtension
    {
        #region Methods

        /// <summary>
        /// The get attribute value.
        /// </summary>
        /// <typeparam name="T"> Type of enum. .</typeparam>
        /// <typeparam name="TExpected"> The expected result. .</typeparam>
        /// <param name="enumeration"> The enumeration. .</param>
        /// <param name="expression"> The expression. .</param>
        /// <returns> The <see cref="TExpected"/>. .</returns>
        public static TExpected GetAttributeValue<T, TExpected>(this System.Enum enumeration, Func<T, TExpected> expression)
                where T : Attribute
        {
            var attribute =
                enumeration.GetType()
                    .GetMember(enumeration.ToString())
                    .FirstOrDefault(member => member.MemberType == MemberTypes.Field)
                    ?.GetCustomAttributes(typeof(T), false)
                    .Cast<T>()
                    .SingleOrDefault();

            return attribute == null ? default(TExpected) : expression(attribute);
        }

        /// <summary>
        /// The GetCharValue.
        /// </summary>
        /// <param name="e">The e<see cref="System.Enum"/>.</param>
        /// <returns>The <see cref="char"/>.</returns>
        public static char GetCharValue(this System.Enum e)
        {
            var type = e.GetType();

            var memInfo = type.GetMember(e.ToString());

            if (memInfo.Length <= 0)
            {
                throw new ArgumentException("Enum " + e + " has no EnumCharValue defined!");
            }

            var attrs = memInfo[0].GetCustomAttributes(false);
            if (attrs.Length <= 0)
            {
                throw new ArgumentException("Enum " + e + " has no EnumCharValue defined!");
            }

            foreach (var stringEnumAttribute in attrs.OfType<CharEnumAttribute>())
            {
                return stringEnumAttribute.Value;
            }

            throw new ArgumentException("Enum " + e + " has no EnumCharValue defined!");
        }

        /// <summary>
        /// The get description.
        /// </summary>
        /// <param name="enumeration"> The enumeration. .</param>
        /// <returns> The <see cref="string"/>. .</returns>
        public static string GetDescription(this System.Enum enumeration)
            => enumeration.GetAttributeValue<DescriptionAttribute, string>(o => o.Description);

        /// <summary>
        /// The get enum.
        /// </summary>
        /// <typeparam name="T"> Type of enum. .</typeparam>
        /// <param name="guid"> The guid. .</param>
        /// <returns> The <see cref="T"/>. .</returns>
        public static T GetEnum<T>(this Guid guid)
        {
            var type = typeof(T);

            if (!type.IsEnum)
            {
                throw new InvalidOperationException();
            }

            foreach (var field in from field in type.GetFields()
                                  let attribute = Attribute.GetCustomAttribute(field, typeof(GuidEnumAttribute)) as GuidEnumAttribute
                                  where attribute != null
                                  where attribute.Guid == guid
                                  select field)
            {
                return (T)field.GetValue(null);
            }

            throw new ArgumentException("Not found.", "guid");
        }

        /// <summary>
        /// The get enum.
        /// </summary>
        /// <typeparam name="T"> Type of enum. .</typeparam>
        /// <param name="value"> The value. .</param>
        /// <returns> The <see cref="T"/>. .</returns>
        public static T GetEnum<T>(this int value)
        {
            var type = typeof(T);

            if (!type.IsEnum)
            {
                throw new InvalidOperationException();
            }

            foreach (var field in from field in type.GetFields()
                                  let attribute = Attribute.GetCustomAttribute(field, typeof(ValueEnumAttribute)) as ValueEnumAttribute
                                  where attribute != null
                                  where attribute.Value == value
                                  select field)
            {
                return (T)field.GetValue(null);
            }

            throw new ArgumentException("Not found.", "value");
        }

        /// <summary>
        /// The get enum.
        /// </summary>
        /// <typeparam name="T"> Type of enum. .</typeparam>
        /// <param name="description"> The description. .</param>
        /// <returns> The <see cref="T"/>. .</returns>
        public static T GetEnum<T>(this string description)
        {
            var type = typeof(T);

            if (!type.IsEnum)
            {
                throw new InvalidOperationException();
            }

            foreach (var field in from field in type.GetFields()
                                  let attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute
                                  where attribute != null
                                  where attribute.Description == description
                                  select field)
            {
                return (T)field.GetValue(null);
            }

            throw new ArgumentException("Not found.", "description");
        }

        /// <summary>
        /// The get guid.
        /// </summary>
        /// <param name="e"> The e. .</param>
        /// <returns> The <see cref="Guid"/>. .</returns>
        public static Guid GetGuid(this System.Enum e)
        {
            var type = e.GetType();

            var memInfo = type.GetMember(e.ToString());

            if (memInfo.Length <= 0)
            {
                throw new ArgumentException("Enum " + e + " has no EnumGuid defined!");
            }

            var attrs = memInfo[0].GetCustomAttributes(false);
            if (attrs.Length <= 0)
            {
                throw new ArgumentException("Enum " + e + " has no EnumGuid defined!");
            }

            foreach (var guidEnumAttribute in attrs.OfType<GuidEnumAttribute>())
            {
                return guidEnumAttribute.Guid;
            }

            throw new ArgumentException("Enum " + e + " has no EnumGuid defined!");
        }

        /// <summary>
        /// The get string value.
        /// </summary>
        /// <param name="e"> The e. .</param>
        /// <returns> The <see cref="Guid"/>. .</returns>
        public static string GetStringValue(this System.Enum e)
        {
            var type = e.GetType();

            var memInfo = type.GetMember(e.ToString());

            if (memInfo.Length <= 0)
            {
                throw new ArgumentException("Enum " + e + " has no EnumStringValue defined!");
            }

            var attrs = memInfo[0].GetCustomAttributes(false);
            if (attrs.Length <= 0)
            {
                throw new ArgumentException("Enum " + e + " has no EnumStringValue defined!");
            }

            foreach (var stringEnumAttribute in attrs.OfType<StringEnumAttribute>())
            {
                return stringEnumAttribute.Value;
            }

            throw new ArgumentException("Enum " + e + " has no EnumStringValue defined!");
        }

        /// <summary>
        /// The GetStringValueToEnum.
        /// </summary>
        /// <typeparam name="T">.</typeparam>
        /// <param name="stringValue">The stringValue<see cref="string"/>.</param>
        /// <returns>The <see cref="T"/>.</returns>
        public static T GetStringValueToEnum<T>(this string stringValue)
        {
            var type = typeof(T);

            if (!type.IsEnum)
            {
                throw new InvalidOperationException();
            }

            foreach (var field in from field in type.GetFields()
                                  let attribute = Attribute.GetCustomAttribute(field, typeof(StringEnumAttribute)) as StringEnumAttribute
                                  where attribute != null
                                  where attribute.Value == stringValue
                                  select field)
            {
                return (T)field.GetValue(null);
            }

            throw new ArgumentException("Not found.", "stringvalue");
        }

        /// <summary>
        /// The get value.
        /// </summary>
        /// <param name="e"> The e. .</param>
        /// <returns> The <see cref="int"/>. .</returns>
        public static int GetValue(this System.Enum e)
        {
            var type = e.GetType();

            var memInfo = type.GetMember(e.ToString());

            if (memInfo.Length <= 0)
            {
                throw new ArgumentException("Enum " + e + " has no EnumValue defined!");
            }

            var attrs = memInfo[0].GetCustomAttributes(false);
            if (attrs.Length <= 0)
            {
                throw new ArgumentException("Enum " + e + " has no EnumValue defined!");
            }

            foreach (var valueEnumAttribute in attrs.OfType<ValueEnumAttribute>())
            {
                return valueEnumAttribute.Value;
            }

            throw new ArgumentException("Enum " + e + " has no EnumValue defined!");
        }

        /// <summary>
        /// The parse enum.
        /// </summary>
        /// <typeparam name="T"> The T. .</typeparam>
        /// <param name="value"> The value. .</param>
        /// <returns>The <see cref="T"/>.</returns>
        public static T ParseEnum<T>(string value) => (T)System.Enum.Parse(typeof(T), value, true);

        #endregion
    }
}

