﻿using System;
namespace Report.Infrastructure.Attributes
{
    public sealed class ValueEnumAttribute : Attribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ValueEnumAttribute"/> class.
        /// </summary>
        /// <param name="value"> The value. </param>
        public ValueEnumAttribute(int value) => this.Value = value;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Value
        /// </summary>
        public int Value { get; private set; }

        #endregion
    }
}

