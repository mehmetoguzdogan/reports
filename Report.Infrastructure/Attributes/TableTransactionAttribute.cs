﻿using System;
using Report.Infrastructure.Const;

namespace Report.Infrastructure.Attributes
{
    public sealed class TableTransactionAttribute : Attribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TableTransactionAttribute"/> class.
        /// </summary>
        /// <param name="transactionName">The transactionName<see cref="string"/></param>
        public TableTransactionAttribute(string transactionName = TableTransactions.SEMI_TRANSACTION) => TransactionName = transactionName;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the TransactionName
        /// </summary>
        public string TransactionName { get; private set; }

        #endregion
    }
}

