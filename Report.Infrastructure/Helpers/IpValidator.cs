﻿using System;
using Report.Infrastructure.Const;
using System.Text.RegularExpressions;

namespace Report.Infrastructure.Helpers
{
    public static class IpValidator
    {
        #region Fields

        /// <summary>
        /// Defines the lazy
        /// </summary>
        private static readonly Lazy<Regex> lazy = new Lazy<Regex>(() => new Regex(InfrastructureConstant.IPV4_REGEX));

        #endregion

        #region Methods

        /// <summary>
        /// The IpValidatorRegex
        /// </summary>
        /// <returns>The <see cref="Regex"/></returns>
        public static Regex IpValidatorRegex() => lazy.Value;

        #endregion
    }
}

