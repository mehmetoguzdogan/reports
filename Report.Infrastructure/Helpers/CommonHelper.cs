﻿using System;
using Microsoft.Extensions.DependencyModel;
using Report.Infrastructure.ComponentModel;
using Report.Infrastructure.Const;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Report.Infrastructure.ViewModel;

namespace Report.Infrastructure.Helpers
{
    public static class CommonHelper
    {
        #region Methods

        /// <summary>
        /// The CreateMD5Hash
        /// </summary>
        /// <param name="value">The value<see cref="string"/></param>
        /// <returns>The <see cref="string"/></returns>
        public static string CreateMD5Hash(this string value)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(value));
            byte[] result = md5.Hash;
            StringBuilder strBuilder = new StringBuilder();

            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// The create salt.
        /// </summary>
        /// <returns> The <see cref="string"/>. </returns>
        public static string CreateSalt()
        {
            var saltBytes = new byte[64];
            var rng = new RNGCryptoServiceProvider();
            rng.GetNonZeroBytes(saltBytes);

            return Convert.ToBase64String(saltBytes);
        }

        /// <summary>
        /// The encode password.
        /// </summary>
        /// <param name="password"> The password. </param>
        /// <param name="passwordSalt"> The password salt. </param>
        /// <returns> The <see cref="string"/>. </returns>
        public static string EncodePassword(string password, string passwordSalt)
        {
            var bytePassword = Encoding.Unicode.GetBytes(password);
            var bytePasswordSalt = Convert.FromBase64String(passwordSalt);
            var byteBuffer = new byte[bytePasswordSalt.Length + bytePassword.Length];
            Buffer.BlockCopy(bytePasswordSalt, 0, byteBuffer, 0, bytePasswordSalt.Length);
            Buffer.BlockCopy(bytePassword, 0, byteBuffer, bytePasswordSalt.Length, bytePassword.Length);
            var byteEncryptSha1 = new SHA1CryptoServiceProvider().ComputeHash(byteBuffer);

            return Convert.ToBase64String(byteEncryptSha1);
        }

        /// <summary>
        /// The get all instances of.
        /// </summary>
        /// <typeparam name="T"> Type of interface. </typeparam>
        /// <returns> The <see cref="IEnumerable{Type}"/>. </returns>
        public static List<T> GetAllInstancesOf<T>()
            => GetAllTypesOf<T>()?.Where(o => !o.IsInterface).Select(o => (T)Activator.CreateInstance(o)).ToList();

        /// <summary>
        /// The get all types of.
        /// </summary>
        /// <typeparam name="T"> Type of interface. </typeparam>
        /// <returns> The <see cref="IEnumerable{T}"/>. </returns>
        public static IEnumerable<Type> GetAllTypesOf<T>()
        {
            var platform = Environment.OSVersion.Platform.ToString();
            var runtimeAssemblyNames = DependencyContext.Default.GetRuntimeAssemblyNames(platform);

            return runtimeAssemblyNames.Select(Assembly.Load)
                    .SelectMany(a => a.ExportedTypes)
                    .Where(t => typeof(T).IsAssignableFrom(t));
        }

        /// <summary>
        /// The get df custom type converter.
        /// </summary>
        /// <param name="type"> The type. </param>
        /// <returns> The <see cref="TypeConverter"/>. </returns>
        public static TypeConverter GetCustomTypeConverter(Type type)
        {
            if (type == typeof(List<int>))
            {
                return new GenericListTypeConverter<int>();
            }

            if (type == typeof(List<decimal>))
            {
                return new GenericListTypeConverter<decimal>();
            }

            if (type == typeof(List<string>))
            {
                return new GenericListTypeConverter<string>();
            }

            return TypeDescriptor.GetConverter(type);
        }

        /// <summary>
        /// The GetPropertyAndValueList
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">The data<see cref="T"/></param>
        /// <returns>The <see cref="List{KeyValueViewModel{string, string}}"/></returns>
        public static List<KeyValueViewModel<string, string>> GetPropertyAndValueList<T>(T data)
        {
            List<KeyValueViewModel<string, string>> list = new List<KeyValueViewModel<string, string>>();

            foreach (var prop in typeof(T).GetProperties())
            {
                list.Add(new KeyValueViewModel<string, string>
                {
                    Key = prop.Name,
                    Value = prop.GetValue(data)?.ToString()
                });
            }

            return list;
        }

        /// <summary>
        /// The get types implementing interface.
        /// </summary>
        /// <typeparam name="T"> Type of interface. </typeparam>
        /// <returns> The <see cref="IEnumerable{Type}"/>. </returns>
        public static IEnumerable<Type> GetTypesImplementingInterface<T>()
            => AppDomain.CurrentDomain.GetAssemblies().SelectMany(o => o.GetTypes())
                .Where(o => typeof(T).IsAssignableFrom(o) && !o.IsInterface);

        /// <summary>
        /// Verifies that a string is in valid e-mail format
        /// </summary>
        /// <param name="email">Email to verify</param>
        /// <returns>true if the string is a valid e-mail address and false if it's not</returns>
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return false;
            }
            email = email.Trim();
            var result = Regex.IsMatch(email, InfrastructureConstant.EMAIL_REGEX);

            return result;
        }

        /// <summary>
        /// The IsValidIp
        /// </summary>
        /// <param name="ipAddress">The ipAddress<see cref="string"/></param>
        /// <returns>The <see cref="bool"/></returns>
        public static bool IsValidIp(string ipAddress) => IpValidator.IpValidatorRegex().Match(ipAddress).Success;

        /// <summary>
        /// The MapProperties
        /// </summary>
        /// <param name="source">The source<see cref="object"/></param>
        /// <param name="target">The target<see cref="object"/></param>
        public static void MapProperties(object source, object target)
        {
            PropertyInfo[] sourceProprties = source.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
            PropertyInfo[] targetProprties = target.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (var sourceProp in sourceProprties)
            {
                object osourceVal = sourceProp.GetValue(source, null);
                var targetProp = targetProprties.FirstOrDefault(x => x.Name == sourceProp.Name);

                if (targetProp != null)
                {
                    targetProp.SetValue(target, osourceVal);
                }
            }
        }

        #endregion
    }
}

