﻿using System;
namespace Report.Infrastructure.ViewModel
{
    public class KeyValueViewModel<TKey, TValue>
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Key
        /// </summary>
        public TKey Key { get; set; }

        /// <summary>
        /// Gets or sets the Value
        /// </summary>
        public TValue Value { get; set; }

        #endregion
    }
}

