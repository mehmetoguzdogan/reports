﻿using System;
using Report.Infrastructure.Entity;

namespace Report.Infrastructure.Repository
{
    public interface IUnitRepository<T, TId> : IReadOnlyRepository<T, TId>, IWriteOnlyUnitRepository<T, TId> where T : IBaseEntity<TId>
    {
    }
}

