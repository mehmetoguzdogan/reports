﻿using System;
using Report.Infrastructure.Entity;

namespace Report.Infrastructure.Repository
{
    public interface IRepository<T, TId> : IReadOnlyRepository<T, TId>, IWriteOnlyRepository<T, TId> where T : IBaseEntity<TId>
    {
    }
}

