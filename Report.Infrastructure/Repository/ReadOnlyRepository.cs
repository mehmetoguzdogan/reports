﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Transactions;
using Report.Infrastructure.Entity;

namespace Report.Infrastructure.Repository
{
    public class ReadOnlyRepository<T, TId> where T : class, IBaseEntity<TId>
    {
        #region Fields

        /// <summary>
        /// Defines the context
        /// </summary>
        protected readonly IContext context;

        /// <summary>
        /// Defines the entities
        /// </summary>
        private DbSet<T> entities;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadOnlyRepository{T,TId}"/> class.
        /// </summary>
        /// <param name="context">The context<see cref="IContext"/></param>
        public ReadOnlyRepository(IContext context)
            => this.context = context;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Table
        /// </summary>
        protected DbSet<T> Table => entities ?? (entities = context.Set<T>());

        #endregion

        #region Methods

        /// <summary>
        /// The create no lock transaction.
        /// </summary>
        /// <returns> The <see cref="TransactionScope"/>. </returns>
        public static TransactionScope CreateNoLockTransaction()
        {
            var options = new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted };
            return new TransactionScope(TransactionScopeOption.Required, options, TransactionScopeAsyncFlowOption.Enabled);
        }

        /// <summary>
        /// The All
        /// </summary>
        /// <returns>The <see cref="IQueryable{T}"/></returns>
        public virtual IQueryable<T> All() => Table;

        /// <summary>
        /// The Any
        /// </summary>
        /// <param name="filter">The filter<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="bool"/></returns>
        public bool Any(Expression<Func<T, bool>> filter)
        {
            using (CreateNoLockTransaction())
            {
                return Table.Any(filter);
            }
        }

        /// <summary>
        /// The AnyAsync
        /// </summary>
        /// <param name="filter">The filter<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="Task{bool}"/></returns>
        public async Task<bool> AnyAsync(Expression<Func<T, bool>> filter)
        {
            using (CreateNoLockTransaction())
            {
                return await Table.AnyAsync(filter);
            }
        }

        /// <summary>
        /// The Count
        /// </summary>
        /// <param name="filter">The filter<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="int"/></returns>
        public int Count(Expression<Func<T, bool>> filter)
        {
            using (CreateNoLockTransaction())
            {
                return Table.Count(filter);
            }
        }

        /// <summary>
        /// The CountAsync
        /// </summary>
        /// <param name="filter">The filter<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="Task{int}"/></returns>
        public async Task<int> CountAsync(Expression<Func<T, bool>> filter)
        {
            using (CreateNoLockTransaction())
            {
                return await Table.CountAsync(filter).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// The Find
        /// </summary>
        /// <param name="match">The match<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="T"/></returns>
        public T Find(Expression<Func<T, bool>> match)
        {
            using (CreateNoLockTransaction())
            {
                return Table.SingleOrDefault(match);
            }
        }

        /// <summary>
        /// The FindAll
        /// </summary>
        /// <param name="match">The match<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="ICollection{T}"/></returns>
        public ICollection<T> FindAll(Expression<Func<T, bool>> match)
        {
            using (CreateNoLockTransaction())
            {
                return Table.Where(match).ToList();
            }
        }

        /// <summary>
        /// The FindAllAsync
        /// </summary>
        /// <param name="match">The match<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="Task{ICollection{T}}"/></returns>
        public async Task<ICollection<T>> FindAllAsync(Expression<Func<T, bool>> match)
        {
            using (CreateNoLockTransaction())
            {
                return await Table.Where(match).ToListAsync();
            }
        }

        /// <summary>
        /// The FindAsync
        /// </summary>
        /// <param name="match">The match<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        public async Task<T> FindAsync(Expression<Func<T, bool>> match)
        {
            using (CreateNoLockTransaction())
            {
                return await Table.SingleOrDefaultAsync(match);
            }
        }

        /// <summary>
        /// The Get
        /// </summary>
        /// <param name="id">The id<see cref="TId"/></param>
        /// <returns>The <see cref="T"/></returns>
        public T Get(TId id)
        {
            using (CreateNoLockTransaction())
            {
                return Table.Find(id);
            }
        }

        /// <summary>
        /// The GetAsync
        /// </summary>
        /// <param name="id">The id<see cref="TId"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        public async Task<T> GetAsync(TId id)
        {
            using (CreateNoLockTransaction())
            {
                return await Table.FindAsync(id);
            }
        }

        #endregion
    }
}

