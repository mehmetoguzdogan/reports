﻿using System;
using System.Data;

namespace Report.Infrastructure.Repository
{
    public interface IDapperRepository
    {
        #region Methods

        /// <summary>
        /// The Query
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="IEnumerable{T}"/></returns>
        IEnumerable<T> Query<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null);

        /// <summary>
        /// The QueryAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{IEnumerable{T}}"/></returns>
        Task<IEnumerable<T>> QueryAsync<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null);

        /// <summary>
        /// The QueryFirst
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="T"/></returns>
        T QueryFirst<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null);

        /// <summary>
        /// The QueryFirstAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        Task<T> QueryFirstAsync<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null);

        /// <summary>
        /// The QueryFirstOrDefault
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="T"/></returns>
        T QueryFirstOrDefault<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null);

        /// <summary>
        /// The QueryFirstOrDefaultAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        Task<T> QueryFirstOrDefaultAsync<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null);

        /// <summary>
        /// The QuerySingle
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="T"/></returns>
        T QuerySingle<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null);

        /// <summary>
        /// The QuerySingleAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        Task<T> QuerySingleAsync<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null);

        /// <summary>
        /// The QuerySingleOrDefault
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="T"/></returns>
        T QuerySingleOrDefault<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null);

        /// <summary>
        /// The QuerySingleOrDefaultAsync
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query">The query<see cref="string"/></param>
        /// <param name="param">The param<see cref="object"/></param>
        /// <param name="commandType">The commandType<see cref="CommandType?"/></param>
        /// <param name="commandTimeout">The commandTimeout<see cref="int?"/></param>
        /// <returns>The <see cref="Task{T}"/></returns>
        Task<T> QuerySingleOrDefaultAsync<T>(string query, object param = null, CommandType? commandType = null, int? commandTimeout = null);

        #endregion
    }
}

