﻿using System;
namespace Report.Infrastructure.Repository
{
    public interface IProcedureManager
    {
        #region Methods

        /// <summary>
        /// The ExecureSqlCommand
        /// </summary>
        /// <param name="sqlQuery">The sqlQuery<see cref="string"/></param>
        /// <param name="timeout">The timeout<see cref="int?"/></param>
        /// <param name="parameters">The parameters<see cref="object[]"/></param>
        /// <returns>The <see cref="int"/></returns>
        int ExecuteSqlCommand(string sqlQuery, int? timeout = null, params object[] parameters);

        #endregion
    }
}

