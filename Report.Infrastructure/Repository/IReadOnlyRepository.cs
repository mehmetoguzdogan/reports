﻿using System;
using System.Linq.Expressions;
using System.Security.Cryptography;
using Report.Infrastructure.Entity;

namespace Report.Infrastructure.Repository
{
    public interface IReadOnlyRepository<T, TId> where T : IBaseEntity<TId>
    {
        #region Methods

        /// <summary>
        /// The All
        /// </summary>
        /// <returns>The <see cref="IQueryable{T}"/></returns>
        IQueryable<T> All();

        /// <summary>
        /// The Any
        /// </summary>
        /// <param name="filter">The filter<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="bool"/></returns>
        bool Any(Expression<Func<T, bool>> filter);

        /// <summary>
        /// The AnyAsync
        /// </summary>
        /// <param name="filter">The filter<see cref="Expression{Func{T, bool}}"/></param>
        /// <returns>The <see cref="Task{bool}"/></returns>
        Task<bool> AnyAsync(Expression<Func<T, bool>> filter);

        /// <summary>
        /// The count.
        /// </summary>
        /// <param name="filter"> The filter. </param>
        /// <returns> The <see cref="int"/>. </returns>
        int Count(Expression<Func<T, bool>> filter);

        /// <summary>
        /// The count async.
        /// </summary>
        /// <param name="filter"> The filter. </param>
        /// <returns> The <see cref="ValueTask"/>. </returns>
        Task<int> CountAsync(Expression<Func<T, bool>> filter);

        /// <summary>
        /// The find.
        /// </summary>
        /// <param name="match"> The match. </param>
        /// <returns> The <see cref="T"/>. </returns>
        T Find(Expression<Func<T, bool>> match);

        /// <summary>
        /// The find all.
        /// </summary>
        /// <param name="match"> The match. </param>
        /// <returns> The <see cref="ICollection{T}"/>. </returns>
        ICollection<T> FindAll(Expression<Func<T, bool>> match);

        /// <summary>
        /// The find all async.
        /// </summary>
        /// <param name="match"> The match. </param>
        /// <returns> The <see cref="Task"/>. </returns>
        Task<ICollection<T>> FindAllAsync(Expression<Func<T, bool>> match);

        /// <summary>
        /// The find async.
        /// </summary>
        /// <param name="match"> The match. </param>
        /// <returns> The <see cref="Task"/>. </returns>
        Task<T> FindAsync(Expression<Func<T, bool>> match);

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id"> The id. </param>
        /// <returns> The <see cref="T"/>. </returns>
        T Get(TId id);

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="id"> The id. </param>
        /// <returns> The <see cref="Task"/>. </returns>
        Task<T> GetAsync(TId id);

        #endregion
    }
}

