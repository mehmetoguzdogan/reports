﻿
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using Report.Infrastructure.Helpers;

namespace Report.Infrastructure.Engine
{
    public class ServiceHost : IServiceHost
    {
        #region Fields

        /// <summary>
        /// Defines the webHost
        /// </summary>
        private readonly IWebHost webHost;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceHost"/> class.
        /// </summary>
        /// <param name="webHost">The webHost<see cref="IWebHost"/></param>
        public ServiceHost(IWebHost webHost) => this.webHost = webHost;

        #endregion

        #region Methods

        /// <summary>
        /// The Create
        /// </summary>
        /// <param name="args">The args<see cref="string[]"/></param>
        /// <param name="options">The options<see cref="Action{KestrelServerOptions}"/></param>
        /// <param name="urls">The urls<see cref="string[]"/></param>
        /// <param name="useContentRoot">The useContentRoot<see cref="string"/></param>
        /// <param name="useIISIntegration">The useIISIntegration<see cref="bool"/></param>
        /// <returns>The <see cref="HostBuilder"/></returns>
        public static HostBuilder Create(string[] args, Action<KestrelServerOptions> options = null, string[] urls = null, string useContentRoot = null, bool useIISIntegration = false)
        {
            ////Getting IStartup implemented class
            var startUp = CommonHelper.GetTypesImplementingInterface<IStartUp>().FirstOrDefault();

            if (startUp == null)
            {
                throw new NotImplementedException("IStartUp not implemented!!!");
            }

            Console.Title = startUp.Namespace;
            var config = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();

            var webHostBuilder = WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(config)
                .UseStartup(startUp);

            webHostBuilder = (options != null) ? webHostBuilder.UseKestrel(options) : webHostBuilder.UseKestrel();


            if (urls != null && urls.Any())
            {
                webHostBuilder = webHostBuilder.UseUrls(urls);
            }

            if (!string.IsNullOrEmpty(useContentRoot))
            {
                webHostBuilder = webHostBuilder.UseContentRoot(useContentRoot);
            }

            if (useIISIntegration)
            {
                webHostBuilder = webHostBuilder.UseIISIntegration();
            }

            return new HostBuilder(webHostBuilder.Build());
        }

        /// <summary>
        /// The Run
        /// </summary>
        public void Run() => webHost.Run();

        #endregion
    }
}

