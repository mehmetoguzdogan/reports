﻿using Microsoft.AspNetCore.Hosting;

namespace Report.Infrastructure.Engine
{
    public class HostBuilder : BuilderBase
    {
        #region Fields

        /// <summary>
        /// Defines the webHost
        /// </summary>
        private readonly IWebHost webHost;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HostBuilder"/> class.
        /// </summary>
        /// <param name="webHost">The webHost<see cref="IWebHost"/></param>
        public HostBuilder(IWebHost webHost) => this.webHost = webHost;

        #endregion

        #region Methods

        /// <summary>
        /// The Build
        /// </summary>
        /// <returns>The <see cref="ServiceHost"/></returns>
        public override ServiceHost Build() => new ServiceHost(this.webHost);

        #endregion
    }
}

