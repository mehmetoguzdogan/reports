﻿using System;
namespace Report.Infrastructure.Engine
{
    public abstract class BuilderBase
    {
        #region Methods

        /// <summary>
        /// The Build
        /// </summary>
        /// <returns>The <see cref="ServiceHost"/></returns>
        public abstract ServiceHost Build();

        #endregion
    }
}

