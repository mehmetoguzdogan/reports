﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Report.Infrastructure.Engine
{
    public interface IDynamicConfiguration
    {
        #region Methods

        /// <summary>
        /// The Configure
        /// </summary>
        /// <param name="app">The app<see cref="IApplicationBuilder"/></param>
        /// <param name="env">The env<see cref="IHostApplicationLifetime"/></param>
        void Configure(IApplicationBuilder app, IHostApplicationLifetime env);

        #endregion
    }
}

