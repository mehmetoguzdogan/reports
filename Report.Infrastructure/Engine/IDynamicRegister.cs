﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Report.Infrastructure.Engine
{
    public interface IDynamicRegister
    {
        #region Methods

        /// <summary>
        /// The Configure
        /// </summary>
        /// <param name="service">The service<see cref="IServiceCollection"/></param>
        void Configure(IServiceCollection service);

        #endregion
    }
}

