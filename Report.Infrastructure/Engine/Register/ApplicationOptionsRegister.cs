﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Report.Infrastructure.Configuration;
using Microsoft.Extensions.Configuration;

namespace Report.Infrastructure.Engine.Register
{
    public static class ApplicationOptionsRegister
    {
        #region Methods

        /// <summary>
        /// The RegisterApplicationOptions
        /// </summary>
        /// <param name="service">The service<see cref="IServiceCollection"/></param>
        /// <param name="configuration">The configuration<see cref="IConfiguration"/></param>
        public static void RegisterApplicationOptions(this IServiceCollection service, IConfiguration configuration)
        {
            ApplicationOptions options = new ApplicationOptions();
            var section = configuration.GetSection("applicationoptions");
            section.Bind(options);
            if (options.ApplicationId == null || options.ApplicationId == Guid.Empty)
            {
                throw new NotImplementedException("ApplicationId required!");
            }
            service.AddSingleton<ApplicationOptions>(options);
        }

        #endregion
    }
}

